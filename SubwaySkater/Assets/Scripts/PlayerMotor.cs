﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour {
    private CharacterController controller;
    private const float LANE_DISTANCE = 3.0f;
    private const float TURN_SPEED = 0.05f;
    private Animator anim;
    //Movement
    private float jumpForce = 6.0f;
    private float gravity = 12.0f;
    private float verticalVelocty;
    private float speed = 7.0f;
    private int desiredLane = 1; //0=Left , 1=Middle, 2=Right

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
        //Gather the inputs on which lane we should be
        //if (Input.GetKeyDown(KeyCode.LeftArrow))
        if (MobileInput.Instance.SwipeLeft)
            MoveLane(false);

        //if (Input.GetKeyDown(KeyCode.RightArrow))
        if (MobileInput.Instance.SwipeRight)
            MoveLane(true);

        //Calculate where we should be in future
        Vector3 targetPosition = transform.position.z * Vector3.forward;
        if (desiredLane == 0)
        {
            targetPosition += Vector3.left * LANE_DISTANCE;
        }
        else if(desiredLane == 2)
        {
            targetPosition += Vector3.right * LANE_DISTANCE;
        }

        //Calculate our Move Delta
        Vector3 moveVector = Vector3.zero; //Initiate
        moveVector.x = (targetPosition - transform.position).normalized.x * speed;
        //Calculate Y
        bool isGrounded = IsGrounded();
        anim.SetBool("Grounded",isGrounded);
        if (isGrounded) //If grounded
        {
            verticalVelocty = -0.1f;
            //if (Input.GetKeyDown(KeyCode.Space))
            if (MobileInput.Instance.SwipeUp)
            {
                //Jump code
                anim.SetTrigger("Jump");
                verticalVelocty = jumpForce;
            }
        }
        else
        {
            verticalVelocty -= (gravity * Time.deltaTime);

            //Fast fall mechanism when we press dow or swipedown
            //if (Input.GetKeyDown(KeyCode.Space))
            if (MobileInput.Instance.SwipeDown)
            {
                verticalVelocty = -jumpForce;
            }
        }
      
        moveVector.y = verticalVelocty;
        moveVector.z = speed;

        //Move the penngu
        controller.Move(moveVector * Time.deltaTime);

        //Rotate our Penngu to where he is going
        Vector3 dir = controller.velocity;
        if(dir != Vector3.zero)
        {
            dir.y = 0;
            transform.forward = Vector3.Lerp(transform.forward, dir, TURN_SPEED);
        }
    }
    private void MoveLane(bool goingRight)
    {
        desiredLane += (goingRight) ? 1 : -1;//If going right add 1 and if going left subtract 1 from desired lane
        desiredLane = Mathf.Clamp(desiredLane, 0, 2);//Will keep the value of desired lane between 0 and 2

    }
    private bool IsGrounded()
    {
        Ray groundRay = new Ray(new Vector3(controller.bounds.center.x,
                                           (controller.bounds.center.y - controller.bounds.extents.y) + 0.1f,
                                           controller.bounds.center.z),
                                Vector3.down);
        Debug.DrawRay(groundRay.origin, groundRay.direction, Color.cyan, 2.0f);
        return (Physics.Raycast(groundRay, 0.2f + 0.1f));
    }
}
