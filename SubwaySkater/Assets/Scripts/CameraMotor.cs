﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour {

    public Transform lookAt; //Our pengu //Object we are looking at
    public Vector3 offset = new Vector3(0f, 5f, -10.0f);

    public void Start()
    {
        transform.position = lookAt.position + offset;
    }
    private void LateUpdate()
    {
        Vector3 desiredPosition = lookAt.position + offset;
        desiredPosition.x = 0; //so that the camera doesn't move with player to left or right
        transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime);
    }
}
